#ifndef CHANGECONTRASTCOMMAND_H
#define CHANGECONTRASTCOMMAND_H

#include <QImage>
#include <QObject>
#include <QUndoStack>
#include <QVector>

class layer;

class changeContrastCommand : public QUndoCommand
{
public:
    changeContrastCommand(layer * _layer, double intensity);

    virtual void redo();
    virtual void undo();

private:
    layer * mLayer;
    QImage mCopy;

    double mIntensity;
};


#endif // CHANGECONTRASTCOMMAND_H
