#ifndef DRAWCOMMAND_H
#define DRAWCOMMAND_H

#include <QImage>
#include <QPoint>
#include <QUndoStack>
#include <QVector>
#include <QColor>

class layer;

class drawCommand : public QUndoCommand
{
public:
    drawCommand(layer * _layer, QString brushType, int brushSize, QColor color);

    void addPoint(QPoint point);

    virtual void redo();
    virtual void undo();

private:
    layer * mLayer;
    QImage mCopy;
    QVector<QPoint> mPoints;

    QString mBrushType;
    int mBrushSize;
    QColor mColor;
};

#endif // DRAWCOMMAND_H
