#include "layersmanager.h"
#include "activeproject.h"
#include "addlayercommand.h"
#include "removelayercommand.h"

#include <QTimer>


layersManager::layersManager(QObject *parent, activeProject * project) :
    SerializableClass(parent),
    mProject(project), mActiveIndex(0), mEditor(this, project)
{
    mScene = new QGraphicsScene;

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(50);
}

void layersManager::draw()
{
    for (auto _layer = mActiveLayers.rbegin(); _layer != mActiveLayers.rend(); _layer++){
        (*_layer)->draw();
    }
}

layer *layersManager::getActive()
{
    if (mActiveLayers.isEmpty()){
        return nullptr;
    }

    return mActiveLayers.at(mActiveIndex);
}

void layersManager::setActive(int index)
{
    mActiveIndex = index;
}

layer * layersManager::addLayer(int index)
{
    auto _layer = new layer(this, mProject, mScene);

    mProject->mUndoStack.push(new addLayerCommand(_layer, index));

    return _layer;
}

void layersManager::restoreLayer(int index, layer *_layer)
{
    mProject->mModel.insertRows(index,1);
    mActiveLayers.insert(index, _layer);

    mProject->mModel.setData(mProject->mModel.index(index), _layer->mName);
}

layer * layersManager::removeLayer(int index)
{
    auto _layer = mActiveLayers[index];

    mProject->mUndoStack.push(new removeLayerCommand(_layer, index));

    return _layer;
}

void layersManager::removeLayer(int index, layer *_layer)
{
    _layer->mName = mProject->mModel.data(mProject->mModel.index(index)).toString();
    mProject->mModel.removeRows(index,1);
    mActiveLayers.remove(index);

    if (mActiveIndex == index){
        mActiveIndex = 0;
    }
}

void layersManager::update()
{
    draw();
    mScene->update();
}


QDataStream &operator<<(QDataStream &out, layersManager &manager)
{
    qint32 layersSize = manager.mActiveLayers.size();

    out << manager.mActiveIndex << layersSize;

    for (auto & _layer : manager.mActiveLayers)
            out << (*_layer);

    return out;
}

QDataStream &operator>>(QDataStream &in, layersManager &manager)
{
    qint32 layersSize;

    in >> manager.mActiveIndex >> layersSize;

    layer loadedLayer;

    for (auto index = layersSize; index; --index){
        in >> loadedLayer;
        auto _layer = new layer(&manager, manager.mProject, manager.mScene);
        (*_layer) << loadedLayer;
        manager.mActiveLayers.append(_layer);
    }

    return in;
}
