#include "activeproject.h"
#include "layersform.h"
#include "ui_layersform.h"

layersForm::layersForm(QWidget *parent, activeProject * project) :
    QWidget(parent),
    ui(new Ui::layersForm),
    mProject(project)
{
    ui->setupUi(this);

    ui->layersListView->setModel(&project->mModel);


    ui->layersListView->setEditTriggers(QAbstractItemView::AnyKeyPressed | QAbstractItemView::DoubleClicked);
}

layersForm::~layersForm()
{
    delete ui;
}

void layersForm::on_selectPushButton_clicked()
{
    if (ui->layersListView->currentIndex().isValid()){
        int row = ui->layersListView->currentIndex().row();
        mProject->mLayersManager.setActive(row);
    }
}

void layersForm::on_deletePushButton_clicked()
{
    if (ui->layersListView->currentIndex().isValid()){
        int row = ui->layersListView->currentIndex().row();
        mProject->mLayersManager.removeLayer(row);
    }
}

void layersForm::on_addPushButton_clicked()
{
    int row = 0;

    if (ui->layersListView->currentIndex().isValid())
        row = ui->layersListView->currentIndex().row();


    mProject->mLayersManager.addLayer(row);

    QModelIndex index = mProject->mModel.index(row);

    ui->layersListView->setCurrentIndex(index);
    mProject->mModel.setData(index, QString("Layer"));
    ui->layersListView->edit(index);
}
