#ifndef LAYER_H
#define LAYER_H

#include <QImage>
#include <QObject>
#include <QPointer>
#include <QString>
#include <QLabel>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>

class activeProject;

class layer : public QObject
{
    Q_OBJECT
public:
    explicit layer(QObject * parent, activeProject * project, QGraphicsScene * scene);
    layer() {}

    void loadImage(QString fileName);

    void draw();
    void setVisibility(bool to);

    void remove(int index);
    void restore(int index);

    QString mName;
    QImage mImage;

    void refresh();

signals:

public slots:

private:
    activeProject * mProject;
    QGraphicsScene * mScene;
    QGraphicsPixmapItem mItem;
    qint8 mIsVisible;
    qint8 mIsOnScene;
    qint8 mHasChanged;

    friend QDataStream & operator<<(QDataStream &out, const layer& _layer);
    friend QDataStream & operator>>(QDataStream &in, layer& _layer);
    friend layer& operator<<(layer& layerLeft, layer& layerRight);
};

QDataStream &operator<<(QDataStream &out, const layer& _layer);
QDataStream & operator>>(QDataStream &in, layer& _layer);
layer& operator<<(layer& layerLeft, layer& layerRight);

#endif // LAYER_H
