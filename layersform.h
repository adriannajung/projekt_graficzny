#ifndef LAYERSFORM_H
#define LAYERSFORM_H

#include <QStringListModel>
#include <QWidget>

namespace Ui {
class layersForm;
}

class activeProject;

class layersForm : public QWidget
{
    Q_OBJECT

public:
    explicit layersForm(QWidget *parent, activeProject * project);
    ~layersForm();

private slots:
    void on_selectPushButton_clicked();

    void on_deletePushButton_clicked();

    void on_addPushButton_clicked();

private:
    Ui::layersForm *ui;
    activeProject * mProject;
};

#endif // LAYERSFORM_H
