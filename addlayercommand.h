#ifndef ADDLAYERCOMMAND_H
#define ADDLAYERCOMMAND_H

#include <QUndoStack>

class layer;

class addLayerCommand : public QUndoCommand
{
public:
    addLayerCommand(layer * _layer, int index);

    virtual void redo();
    virtual void undo();

private:
    layer * mLayer;
    int mIndex;
};

#endif // ADDLAYERCOMMAND_H
