#include "removelayercommand.h"
#include "layer.h"

removeLayerCommand::removeLayerCommand(layer * _layer, int index) :
    mLayer(_layer),
    mIndex(index)
{
}

void removeLayerCommand::redo()
{
    mLayer->remove(mIndex);
}

void removeLayerCommand::undo()
{
    mLayer->restore(mIndex);
}
